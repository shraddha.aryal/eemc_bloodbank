package com.example.bloodbank.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.*
import android.widget.Toast
import com.example.bloodbank.Utils.Constants
import com.example.bloodbank.global.LocationVariables
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import java.util.*


object LocationUtils {
    @SuppressLint("MissingPermission")
    fun getCurrentLatLong(context: Context) {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
         locationManager.requestLocationUpdates(
             LocationManager.GPS_PROVIDER,
             2000,
             5f,
             LocationListener {
                 LocationVariables.latitude = it.latitude
                 LocationVariables.longitude = it.longitude
                 LocationVariables.mutableLatitude.postValue(it.latitude)
                 LocationVariables.mutableLongitude.postValue(it.longitude)
             })
    }

    fun getCurretntLocation(context: Context, latitude: Double, longitude: Double): String {
        val geocoder: Geocoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address> = geocoder.getFromLocation(
            latitude,
            longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        val address: String = addresses[0]
            .getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        val city: String = addresses[0].locality
        val state: String = addresses[0].adminArea
        val country: String = addresses[0].countryName
        val postalCode: String = addresses[0].postalCode
        val knownName: String =
            addresses[0].featureName // Only if available else return NULL
        return address
    }

    fun getDirection(origin: LocationDAO, destination: LocationDAO):String{
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}&destination=${destination.latitude},${destination.longitude}&key=${Constants.MAP_API_KEY}"
        //return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}&destination=${destination.latitude},${destination.longitude}&sensor=false&mode=driving}"
    }

    fun getDistance(location: LatLng):Boolean{
        val distance = FloatArray(2)
        val circleOptions =
            CircleOptions().center(LatLng(LocationVariables.latitude,LocationVariables.longitude)).radius(LocationVariables.searchRadius)
        Location.distanceBetween( location.latitude, location.longitude,
            circleOptions.center.latitude, circleOptions.center.longitude, distance)
        return distance[0] <= circleOptions.radius
    }

}