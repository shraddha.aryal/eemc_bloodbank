package com.example.bloodbank.login.contracts

interface LoginViewModelContracts {
    fun onLoginClicked()

    fun onSignupClicked()
}