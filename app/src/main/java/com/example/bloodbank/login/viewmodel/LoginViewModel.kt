package com.example.bloodbank.login.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bloodbank.Utils.InputValidation
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.login.contracts.LoginViewModelContracts
import com.example.bloodbank.login.repository.LoginRepository
import com.google.firebase.auth.AuthResult
import kotlinx.coroutines.launch

class LoginViewModel @ViewModelInject constructor(private val loginRepository: LoginRepository) :
    ViewModel(), LoginViewModelContracts {
    var checkIsInputEmpty: MutableLiveData<Boolean> = MutableLiveData(false)
    var checkPasswordIsEmpty: MutableLiveData<Boolean> = MutableLiveData(false)
    var isLoginWithPhoneNumber: MutableLiveData<String> = MutableLiveData("")
    var onSingUpClickObserver: MutableLiveData<Boolean> = MutableLiveData(false)
    var userAuthenticationResponse: MutableLiveData<Resource<AuthResult>> =
        loginRepository.userAuthenticationResponse
    var numberNotRegisteredMessage :MutableLiveData<Resource<String>> = MutableLiveData()
    var observableUserId: String = ""
    var observablePassword: String = ""
    var invalidUserIdObserver: MutableLiveData<Boolean> = MutableLiveData(false)

    override fun onLoginClicked() {
        numberNotRegisteredMessage.postValue(Resource(Status.LOADING,null,null))
        viewModelScope.launch { authenticateUser(observableUserId, observablePassword) }
    }

    override fun onSignupClicked() {
        if (onSingUpClickObserver.value == false) {
            onSingUpClickObserver.postValue(true)
        }
    }

    private suspend fun authenticateUser(userId: String, password: String) {
        //the code will enter this if block if any of the given field .i.e userId or password is empty
        if (InputValidation.checkIsInputEmpty(userId)) {
            checkIsInputEmpty.postValue(InputValidation.checkIsInputEmpty(userId))
        }
        //the code will enter this else block if the userId and password both are not empty
        else {
            //the code will enter this if block only if the given user id is either valid email or valid phone number

            if (InputValidation.checkIfEmailIsValid(userId) ||
                InputValidation.checkIfNumberIsValid(userId)
            ) {
                //the code will enter this if block if the user id is a valid email
                if (InputValidation.checkIfEmailIsValid(userId)) {
                    if(InputValidation.checkIsInputEmpty(password)){
                        checkPasswordIsEmpty.postValue(InputValidation.checkIsInputEmpty(password))
                    }else{
                        loginRepository.authenticateUser(userId, password)
                    }
                }
                //the code will enter this else block if the user id is a valid phone number
                else {
                    var isNumberRegistered = loginRepository.checkIfNumberIsRegistered(userId)
                    if(isNumberRegistered){
                        numberNotRegisteredMessage.postValue(Resource(Status.SUCCESS,null,userId))
                        isLoginWithPhoneNumber.postValue(userId)
                    }else{
                        numberNotRegisteredMessage.postValue(Resource(Status.ERROR,null,"Phone number is not registered. Please Register the phone number first"))
                    }
                }
            }
            //the code will enter this  else block will run if the given user id is invalid in both number and email validation
            else {
                invalidUserIdObserver.postValue(true)
            }
        }
    }

    fun fetchUserAuthenticationResponse(): LiveData<Resource<AuthResult>> =
        userAuthenticationResponse

}