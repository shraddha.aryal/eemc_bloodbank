package com.example.bloodbank.login.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.global.UserVariables
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class LoginRepository @Inject constructor(
    private val mAuth: FirebaseAuth,
    private val databaseReference: DatabaseReference
) {
    var userAuthenticationResponse: MutableLiveData<Resource<AuthResult>> = MutableLiveData()
    var isNumberRegisteredMutableData: MutableLiveData<Boolean> = MutableLiveData(false)

    fun authenticateUser(userId: String, password: String) {
        userAuthenticationResponse.postValue(Resource(Status.LOADING, null, null))
        mAuth.signInWithEmailAndPassword(userId, password).addOnCompleteListener {
            if (it.isSuccessful) {
                userAuthenticationResponse.postValue(
                    Resource(
                        Status.SUCCESS,
                        it.result,
                        "User logged in successfully."
                    )
                )
                UserVariables.userId = userId

            } else {
                userAuthenticationResponse.postValue(
                    Resource(
                        Status.ERROR,
                        null,
                        it.exception!!.localizedMessage.toString()
                    )
                )
            }
        }
    }

    suspend fun checkIfNumberIsRegistered(userId: String):Boolean{
        return suspendCoroutine { continuation ->
            databaseReference.child("Registration")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        continuation.resume(dataSnapshot.hasChild(userId))
                    }
                    override fun onCancelled(error: DatabaseError) {
                        continuation.resume(false)
                    }
                })
        }



    }

    fun fetchIsNumberRegisteredResponse() = isNumberRegisteredMutableData
}


