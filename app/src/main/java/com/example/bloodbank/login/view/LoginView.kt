package com.example.bloodbank.login.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import com.example.bloodbank.MainActivity
import com.example.bloodbank.R
import com.example.bloodbank.Utils.Constants.PHONE_NUMBER

import com.example.bloodbank.Utils.SnackBar
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.dashboard.DashboardActivity
import com.example.bloodbank.databinding.ActivityLoginViewBinding
import com.example.bloodbank.location.LocationUtils
import com.example.bloodbank.login.viewmodel.LoginViewModel
import com.example.bloodbank.registration.view.RegistrationView
import com.example.bloodbank.verification.view.VerificationView
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * This class is responsible for authenticating the user
 * If the user is authentic user , will proceed toward home page
 * Else will have option to signup aswell
 * */
@AndroidEntryPoint
class LoginView : AppCompatActivity() {


    lateinit var binding: ActivityLoginViewBinding
    private val loginViewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var mAuth: FirebaseAuth
    private var globalVerificationId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Binding the view with xml
        binding = ActivityLoginViewBinding.inflate(layoutInflater)
        binding.viewModel = loginViewModel
        setContentView(binding.root)
        initObserver()

    }

    private fun initObserver() {
        loginViewModel.numberNotRegisteredMessage.observe(this){
            when(it.status){
                Status.SUCCESS->{
                    hideProgressBar()
                    if(it.message.toString().isNotEmpty()){
                        navigateToVerification(it.message.toString())
                    }
                }
                Status.LOADING->{
                    showProgressBar()
                }
                Status.ERROR->{
                    hideProgressBar()
                    if(!it.message.isNullOrEmpty()){
                        SnackBar.showSnackBar(
                            it.message.toString(),
                            binding.coordinatorLayoutLogin,
                            getColor(R.color.errorColor),
                            getColor(R.color.headerColor)
                        )
                    }
                }
            }
        }
        loginViewModel.isLoginWithPhoneNumber.observe(this) {
            if (it.isNotEmpty()) {
                navigateToVerification(it)
            }
        }
        loginViewModel.checkIsInputEmpty.observe(this) {
            if (it) binding.etUserId.error =
                getString(R.string.userid_empty) else binding.etUserId.error = null
        }
        loginViewModel.checkPasswordIsEmpty.observe(this) {
            if (it) binding.etPassword.error =
                getString(R.string.password_empty) else binding.etPassword.error = null
        }
        loginViewModel.onSingUpClickObserver.observe(this) {
            if (it) {
                navigateToRegistration()
            }
        }
        loginViewModel.invalidUserIdObserver.observe(this) {
            if (loginViewModel.checkIsInputEmpty.value == false) {
                if (it) binding.etUserId.error =
                    getString(R.string.invalid_user_id) else binding.etUserId.error = null
            }
        }
        loginViewModel.fetchUserAuthenticationResponse().observe(this) {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        hideProgressBar()
                        navigateToMainpage()
                    }
                    Status.ERROR -> {
                        hideProgressBar()
                        SnackBar.showSnackBar(
                            it.message.toString(),
                            binding.coordinatorLayoutLogin,
                            getColor(R.color.errorColor),
                            getColor(R.color.headerColor)
                        )
                    }
                    Status.LOADING -> {
                        showProgressBar()
                    }
                }
            }
        }
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun navigateToMainpage() {
        startActivity(Intent(this,DashboardActivity::class.java))
    }

    override fun onResume() {
        super.onResume()
        loginViewModel.onSingUpClickObserver.postValue(false)
        binding.etUserId.setText("")
        binding.etUserId.clearFocus()
        binding.etPassword.setText("")
        binding.etPassword.clearFocus()
        loginViewModel.isLoginWithPhoneNumber.postValue("")
        loginViewModel.checkPasswordIsEmpty.postValue(false)
        loginViewModel.checkIsInputEmpty.postValue(false)
    }


    private fun navigateToRegistration() {
        val intent = Intent(this, RegistrationView::class.java)
        startActivity(intent)  //starting the verification page
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left) // adding animation
    }

    /**
     * Function that navigate to registration page
     * */
    fun navigateToVerification(number: String) {
        //adding the response from api to intent to pass it to verification page
        val intent = Intent(this, VerificationView::class.java)
        intent.putExtra(PHONE_NUMBER, number)
        startActivity(intent)  //starting the verification page
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left) // adding animation
    }


}