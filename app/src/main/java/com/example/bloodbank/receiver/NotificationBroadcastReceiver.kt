package com.example.bloodbank.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.bloodbank.Services.NotificationService


class NotificationBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        context.startService(Intent(context, NotificationService::class.java))
    }
}