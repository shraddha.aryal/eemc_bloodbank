package com.example.bloodbank.network

data class Result (val routes:List<Route>, val status:String)