package com.example.bloodbank.Services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.example.bloodbank.Utils.Constants
import com.example.bloodbank.Utils.NotificationUtils
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.receiver.NotificationBroadcastReceiver
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


@AndroidEntryPoint
class NotificationService : Service() {

    @Inject
    lateinit var preferenceManager: PreferenceManager
    @Inject
    lateinit var databaseReference: DatabaseReference
    @Inject
    @ApplicationContext
    lateinit var mContext: Context

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startObserveingRequest()
        return START_NOT_STICKY
    }

    private fun startObserveingRequest() {
        val userDetails = preferenceManager.retrieveUserDetails()
        databaseReference.child(Constants.REQUEST).child(userDetails.userUniqueId!!)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.value != null) {
                        var donor = snapshot.getValue(User::class.java)
                        if(donor!=null){

                                val notification =
                                    NotificationUtils.showRequestNotification(mContext, donor)
                                startForeground(1, notification)
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            })
        databaseReference.child("Cancelled").child(preferenceManager.retrieveUniqueId()!!).addValueEventListener(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var user = snapshot.getValue(User::class.java)
                if(user?.donorName!=null){
                    val notification =
                        NotificationUtils.showCancelNotification(mContext, user?.donorName!!)
                    startForeground(1, notification)
                }
                databaseReference.child("Cancelled").child(preferenceManager.retrieveUniqueId()!!).removeValue()
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
        databaseReference.child("Accepted").child(preferenceManager.retrieveUniqueId()!!).addValueEventListener(object:ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var user = snapshot.getValue(User::class.java)
                if(user!=null){
                    val notification =
                        NotificationUtils.showAcceptedNotification(mContext, user)
                    startForeground(1, notification)
                }

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        val broadcastIntent = Intent(this, NotificationBroadcastReceiver::class.java)
        sendBroadcast(broadcastIntent)
    }


    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }
}