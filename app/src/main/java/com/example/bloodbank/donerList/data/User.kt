package com.example.bloodbank.donerList.data

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable


data class User (
    var bloodGroup: String? = null,
    var lastDonated: String? = "N/A",
    var location: String? = null,
    var username: String? = null,
    var contactNumber: String? = null,
    var userId: String? = null,
    var userUniqueId: String? = null,
    var donor: Boolean = false,
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var donationCount:Int = 0,
    var isRequestCancelled: Boolean = false,
    var donorName :String = ""
):Serializable,Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(bloodGroup)
        parcel.writeString(lastDonated)
        parcel.writeString(location)
        parcel.writeString(username)
        parcel.writeString(contactNumber)
        parcel.writeString(userId)
        parcel.writeString(userUniqueId)
        parcel.writeByte(if (donor) 1 else 0)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeInt(donationCount)
        parcel.writeByte(if (isRequestCancelled) 1 else 0)
        parcel.writeString(donorName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}