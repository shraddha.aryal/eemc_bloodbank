package com.example.bloodbank.donerList.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.donerList.repository.DonorsListRepository

class DonorsViewModel @ViewModelInject constructor(private val donorsListRepository: DonorsListRepository) :
    ViewModel() {
    private var userList: MutableLiveData<Resource<MutableList<User>>> =
        donorsListRepository.fetchDonorList()
    var onBackPressedObserver: MutableLiveData<Boolean> = MutableLiveData(false)


    var donorListSize: Int = 0

    fun retrieveDonorList(bloodGroup: String) {
        donorsListRepository.retrieveDonorList(bloodGroup)
    }


    fun fetchDonorList(): LiveData<Resource<MutableList<User>>> = userList

    fun onBackPressed() {
        if (onBackPressedObserver.value == false) {
            onBackPressedObserver.postValue(true)
        }
    }

    fun requestForBlood(userDetails: User?) {
        donorsListRepository.requestBlood(userDetails)
    }

    fun fetchRequesForBloodResponse(): LiveData<Resource<String>> =
        donorsListRepository.fetchResponseFromRequestForBlood()
}