package com.example.bloodbank.donerList.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bloodbank.databinding.DonorListRvContainerBinding
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.donerList.view.DonorsView
import com.example.bloodbank.location.LocationUtils
import com.google.android.gms.maps.model.LatLng

class  DonorListAdapter(
    private val context: DonorsView,
    private val data: MutableList<User>?
) : RecyclerView.Adapter<DonorViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DonorViewHolder {
        val binding = DonorListRvContainerBinding.inflate(LayoutInflater.from(context),parent,false)
        return DonorViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    override fun onBindViewHolder(holder: DonorViewHolder, position: Int) {
        val donor = data?.get(position)
            donor?.location = LocationUtils.getCurretntLocation(context,donor!!.latitude,donor!!.longitude)
            holder.binding.donerObject  = donor
            holder.binding.executePendingBindings()
            holder.binding.btnRequest.setOnClickListener {
                context.requestForBlood(data?.get(position))
            }
    }
}

class DonorViewHolder( val binding: DonorListRvContainerBinding) :RecyclerView.ViewHolder(binding.root)


