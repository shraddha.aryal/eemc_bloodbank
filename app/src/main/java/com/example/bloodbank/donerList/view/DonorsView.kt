package com.example.bloodbank.donerList.view

import android.content.Intent
import android.location.LocationListener
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bloodbank.R
import com.example.bloodbank.Utils.Constants

import com.example.bloodbank.Utils.CustomProgressDialog
import com.example.bloodbank.Utils.SnackBar
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.databinding.ActivityDonersListBinding
import com.example.bloodbank.donerList.adapter.DonorListAdapter
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.donerList.viewModel.DonorsViewModel
import com.example.bloodbank.location.LocationUtils
import com.example.bloodbank.maps.enum.MapEnum
import com.example.bloodbank.maps.view.MapActivity
import com.example.bloodbank.maps.view.MultipleMarkerMapActivity
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DonorsView : AppCompatActivity() ,LocationListener{
    private lateinit var donorList: ArrayList<User>
    private lateinit var binding:ActivityDonersListBinding
    private val donorsViewModel:DonorsViewModel by viewModels()
    private lateinit var location:String
    private lateinit var bloodGroup:String
    private var tvShowMapWidth = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        location = intent.getStringExtra(Constants.LOCATION).toString()
        bloodGroup = intent.getStringExtra(Constants.BLOOD_GROUP).toString()
        binding = ActivityDonersListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@DonorsView)
        }
        binding.viewModel = donorsViewModel

        tvShowMapWidth = binding.tvShowMapText.width.toFloat()
        initObserver()
        initlistener()

    }

    private fun initlistener() {
        binding.cvShowMap.setOnLongClickListener {
            alterTheTextVisibilityOnShowMapCard()
            true
        }
        binding.cvShowMap.setOnClickListener {
            startMapActivity()
            true
        }
    }

    private fun startMapActivity() {
        var mapIntent = Intent(this, MultipleMarkerMapActivity::class.java)
        mapIntent.putParcelableArrayListExtra(MapEnum.UserList.value, donorList)
        startActivity(mapIntent)
    }

    private fun alterTheTextVisibilityOnShowMapCard() {
        if (binding.tvShowMapText.visibility == View.VISIBLE) binding.tvShowMapText.visibility =
            View.GONE else binding.tvShowMapText.visibility = View.VISIBLE
        binding.tvShowMapText.animate().translationX(tvShowMapWidth)
    }

    private fun initObserver() {
        donorsViewModel.fetchDonorList().observe(this){
            it?.let { resource ->
                when (resource.status) {
                    //if status is successful
                    Status.SUCCESS -> {
                        if(it.data!=null && it.data.isNotEmpty()){
                            donorList = it.data as ArrayList<User>
                            binding.bloodType = bloodGroup
                            donorsViewModel.donorListSize = it.data.size
                            dataLoadedUI()
                            setAdapter(it.data)
                        }else{
                            noDataUI()
                        }

                    }
                    //if status is error
                    Status.ERROR -> {
                        CustomProgressDialog.dialog.dismiss()
                    }
                    //if status is loading
                    Status.LOADING -> {

                    }
                }
            }

        }
        donorsViewModel.onBackPressedObserver.observe(this){
            if(it){
                closeDonorView()
            }
        }

        donorsViewModel.fetchRequesForBloodResponse().observe(this){
            it?.let { resource ->
                when (resource.status) {
                    //if status is successful
                    Status.SUCCESS -> {
                        hideProgressBar()
                        SnackBar.showSnackBar(it.message!!,binding.coordinatorLayout)
                    }
                    //if status is error
                    Status.ERROR -> {
                        hideProgressBar()
                    }
                    //if status is loading
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                }
            }
        }
    }

    private fun showProgressDialog() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility =View.GONE
    }


    private fun noDataUI() {
        binding.llNoDonors.visibility = View.VISIBLE
        binding.llDataPreview.visibility = View.GONE
    }

    private fun dataLoadedUI() {
        binding.tvUserCountDetails.visibility = View.VISIBLE
        binding.llNoDonors.visibility =View.GONE
        binding.llDataPreview.visibility =View.VISIBLE
        binding.shimmer.visibility = View.GONE
        binding.shimmerFrameLayout.stopShimmerAnimation()
        binding.recyclerView.visibility =View.VISIBLE
    }

    private fun setAdapter(data: MutableList<User>?) {
        var filteredList  = ArrayList<User>()
        if (data != null) {
            for(item in data){
                if(LocationUtils.getDistance(LatLng(item.latitude,item.longitude))){
                    filteredList.add(item)
                }
            }
        }
        binding.donerListSize = filteredList.size
        if(filteredList.isNotEmpty()){
            binding.cvShowMap.visibility = View.VISIBLE
        }else{
            noDataUI()
        }
        val donorAdapter = DonorListAdapter(this,filteredList)
        binding.recyclerView.adapter = donorAdapter
    }

    override fun onResume() {
        super.onResume()
        donorsViewModel.retrieveDonorList(bloodGroup!!)
        binding.shimmer.visibility = View.VISIBLE
        binding.tvUserCountDetails.visibility = View.GONE
        binding.shimmerFrameLayout.startShimmerAnimation()
    }

    override fun onBackPressed() {
        closeDonorView()
    }

    private fun closeDonorView() {
            this.finish()       //closing the activity
            overridePendingTransition(
                R.anim.slide_in_left, R.anim.slide_out_right
            )
    }

    fun requestForBlood(userDetails: User?) {
        donorsViewModel.requestForBlood(userDetails)
    }

    override fun onLocationChanged(p0: android.location.Location) {
        TODO("Not yet implemented")
    }


}