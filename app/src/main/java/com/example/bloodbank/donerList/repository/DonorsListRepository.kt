package com.example.bloodbank.donerList.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.bloodbank.Utils.Constants
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.global.LocationVariables
import com.example.bloodbank.location.LocationUtils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


class DonorsListRepository @Inject constructor(private val databaseReference: DatabaseReference,private val preferenceManager: PreferenceManager,@ApplicationContext private var mContext: Context) {
    private val userList: MutableList<User> = mutableListOf()
    private var mutableUserListData: MutableLiveData<Resource<MutableList<User>>> = MutableLiveData()
    private val apiResponseMessage : MutableLiveData<Resource<String>> = MutableLiveData()

    fun retrieveDonorList( bloodGroup: String) {
        // Read from the database
        mutableUserListData.postValue(Resource(Status.LOADING,null,""))
        // Read from the database
        databaseReference.child("DonorList").child(bloodGroup).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                userList.clear()
                if(dataSnapshot.exists()){
                    for(donorDetails in dataSnapshot.children){
                        if(preferenceManager.retrieveUniqueId()!=donorDetails.key){
                            var donor = donorDetails.getValue(User::class.java)
                            userList.add(donor!!)
                        }
                    }
                }
                mutableUserListData.postValue(Resource(Status.SUCCESS,userList,"Success"))

            }
            override fun onCancelled(error: DatabaseError) {
                Log.e("value",error.toString())
            }
        })
    }

    fun fetchDonorList():MutableLiveData<Resource<MutableList<User>>> = mutableUserListData

    fun requestBlood(donor: User?) {
        apiResponseMessage.postValue(Resource(Status.LOADING , "loading","Requesting For Blood. Please Wait..."))
        val userDetails = preferenceManager.retrieveUserDetails()
        userDetails.latitude = LocationVariables.latitude
        userDetails.longitude = LocationVariables.longitude
        userDetails.donorName = donor?.username!!
        userDetails.location = LocationUtils.getCurretntLocation(mContext,LocationVariables.latitude,LocationVariables.longitude)
        databaseReference.child(Constants.REQUEST).child(donor?.userUniqueId!!).setValue(userDetails).addOnCompleteListener {
            if(it.isSuccessful){
                apiResponseMessage.postValue(Resource(Status.SUCCESS , "success","Your request has been successfully sent."))
            }
        }
    }

    fun fetchResponseFromRequestForBlood():MutableLiveData<Resource<String>> = apiResponseMessage

}