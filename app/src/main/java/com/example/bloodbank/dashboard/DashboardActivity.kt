package com.example.bloodbank.dashboard

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.example.bloodbank.R
import com.example.bloodbank.Services.NotificationService
import com.example.bloodbank.Utils.DonationForm
import com.example.bloodbank.Utils.PermissionUtilites
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.dashboard.viewModel.DashboardViewModel
import com.example.bloodbank.databinding.ActivityDashboardBinding
import com.example.bloodbank.fragments.findDonors.view.FindDonorsFragment
import com.example.bloodbank.fragments.request.RequestFragment
import com.example.bloodbank.fragments.settings.SettingsFragment
import com.example.bloodbank.fragments.userProfile.UserProfileFragment
import com.example.bloodbank.global.LocationVariables
import com.example.bloodbank.location.LocationUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DashboardActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: ActivityDashboardBinding
    private val dashboardViewModel: DashboardViewModel by viewModels()
    @Inject  lateinit var preferenceManager: PreferenceManager

    //Initializing all fragments
    private var findDonorsFragment: Fragment = FindDonorsFragment()
    private var requestFragment: Fragment = RequestFragment()
    private var userProfileFragment: Fragment = UserProfileFragment()

    /*private var contactsFragment: Fragment = ContactsFragment()
    private var favoritesFragment: Fragment = FavoritesFragment()
    private var historyFragment: Fragment = HistoryFragment()
    private var settingFragment: Fragment = SettingFragment()*/
    private var settingFragment: Fragment = SettingsFragment()
    private var activeFragment: Fragment = findDonorsFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retrieveUserDetails()
        checkDonationFormFilledStatus()
        if (!PermissionUtilites.isLocationPermissionGiven(this)) {
            PermissionUtilites.checkForPermission(this)
        } else {
            LocationUtils.getCurrentLatLong(context = this)
            startNotificationService()
        }
        initViewBinding()
        setContentView(binding.root)
        setupFragment()
        loadFragment(activeFragment)
        initObserver()
        binding.fab.setOnClickListener {
            DonationForm.showDonationForm(this,preferenceManager)
        }
    }

    private fun checkDonationFormFilledStatus() {
        var isDonationFormFilled = preferenceManager.retrieveDonationFormFilledStatus()
        if(!isDonationFormFilled){
            showDonationForm()
        }
    }

    private fun showDonationForm() {
        DonationForm.showDonationForm(this,preferenceManager)
    }

    private fun initObserver() {

        LocationVariables.mutableLatitude.observe(this){
            dashboardViewModel.updateLatitude(it)
        }
        LocationVariables.mutableLongitude.observe(this){
            dashboardViewModel.updateLongitude(it)
        }
    }

    private fun retrieveUserDetails() {
        dashboardViewModel.retrieveUserDetails()
    }


    private fun startNotificationService() {
        startService(Intent(this, NotificationService::class.java))
    }


    private fun initViewBinding() {
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        binding.bottomNavigationView.background = null
        binding.bottomNavigationView.background = null
        binding.bottomNavigationView.menu.getItem(2).isEnabled = false
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(this)
    }

    //setting up the fragment view
    private fun setupFragment() {
        supportFragmentManager
            .beginTransaction()
            .apply {
                add(
                    R.id.frameLayout,
                    userProfileFragment,
                    "UserProfileFragment"
                ).hide(userProfileFragment)
                add(
                    R.id.frameLayout,
                    findDonorsFragment,
                    "FindDonorsFragment"
                ).hide(findDonorsFragment)
                add(
                    R.id.frameLayout,
                    requestFragment,
                    "FindDonorsFragment"
                ).hide(requestFragment)
                add(
                    R.id.frameLayout,
                    settingFragment,
                    "Settings Fragment"
                ).hide(settingFragment)
            }.commit()
    }

    //loading the fragment on tab pressed
    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            supportFragmentManager.beginTransaction().hide(activeFragment).show(fragment).commit()
            activeFragment = fragment
            return true
        }
        return false
    }

    //Navigation controller listener for tab change
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        when (item.itemId) {
            R.id.nav_find_donors -> {
                fragment = findDonorsFragment
                true
            }
            R.id.nav_request -> {
                fragment = requestFragment
                true
            }
            R.id.nav_settings -> {
                fragment = settingFragment
                true
            }
            R.id.profile -> {
                fragment = userProfileFragment
                true
            }
        }
        return loadFragment(fragment)
    }

    /**
     * result observer when the user interact with the permissions for web rtc
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissionsList: Array<String>,
        grantResults: IntArray
    ) {
        /**
         * If permission declined then stop ringtone and closing the activity
         * else checking calling status and proceeding accordingly
         * */
        when (requestCode) {
            100 -> {
                val isMissingPermission: Boolean =
                    grantResults.isEmpty() || grantResults.any { PackageManager.PERMISSION_GRANTED != it }
                if (!isMissingPermission) {
                    LocationUtils.getCurrentLatLong(context = this)
                    startNotificationService()
                }
            }
        }
    }
}