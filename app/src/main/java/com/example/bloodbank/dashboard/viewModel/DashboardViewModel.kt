package com.example.bloodbank.dashboard.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.dashboard.repository.DashboardRepository
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.donerList.repository.DonorsListRepository

class DashboardViewModel @ViewModelInject constructor(private val dashboardRepository: DashboardRepository) :
    ViewModel() {
    fun retrieveUserDetails() {
        dashboardRepository.retrieveUserDetails()
    }

    fun updateLatitude(it: Double?) {
        dashboardRepository.updateLatitude(it)
    }

    fun updateLongitude(it: Double?) {
        dashboardRepository.updateLongitude(it)

    }

}