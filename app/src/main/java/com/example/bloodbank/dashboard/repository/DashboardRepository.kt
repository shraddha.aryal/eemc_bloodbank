package com.example.bloodbank.dashboard.repository

import android.content.Context
import android.util.Log
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.global.UserVariables
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class DashboardRepository @Inject constructor(
    private val databaseReference: DatabaseReference,
    private val preferenceManager: PreferenceManager,
    @ApplicationContext private var mContext: Context
) {
    fun retrieveUserDetails() {
        databaseReference.child("Registration").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (snapshot.exists()) {
                    for (userDetails in snapshot.children) {
                        if(userDetails.child("userId").value==UserVariables.userId){
                            var userDetails =userDetails.getValue(User::class.java)!!
                            preferenceManager.saveUserDetails(userDetails)
                        }
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.e("value", error.toString())
            }
        })
    }

    fun updateLatitude(it: Double?) {
        var myDonorReference = databaseReference.child("DonorList").child(preferenceManager.retrieveUserDetails().bloodGroup!!)
        myDonorReference.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.hasChild(preferenceManager.retrieveUniqueId()!!)){
                    myDonorReference.child(preferenceManager.retrieveUniqueId()!!).child("latitude").setValue(it)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

    }

    fun updateLongitude(it: Double?) {
        var myDonorReference = databaseReference.child("DonorList").child(preferenceManager.retrieveUserDetails().bloodGroup!!)
        myDonorReference.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.hasChild(preferenceManager.retrieveUniqueId()!!)){
                    myDonorReference.child(preferenceManager.retrieveUniqueId()!!).child("longitude").setValue(it)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }
}

