package com.example.bloodbank.fragments.userProfile

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.databinding.FragmentUserProfileBinding
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.location.LocationUtils
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import javax.inject.Inject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [UserProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class UserProfileFragment : Fragment() {
    private lateinit var user: User
    private lateinit var binding: FragmentUserProfileBinding
    @Inject
    lateinit var storageReference: StorageReference



    @Inject
    lateinit var databaseReference: DatabaseReference

    @Inject
    lateinit var preferenceManager: PreferenceManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setFullScreenWindow()
        retrieveUserDetails()

        // Inflate the layout for this fragment
        binding = FragmentUserProfileBinding.inflate(inflater, container, false)
        //binding.viewModel = findDonorsViewModel
        return binding.root
    }


    private fun retirveVerificationCard() {
        var pathReference = storageReference.child("${user.contactNumber!!}.jpg")
        val localFile  = File.createTempFile("verificationImage",".jpg")
        pathReference.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            binding.ivVerficationCard.setImageBitmap(bitmap)
        }.addOnFailureListener{

        }
    }


    private fun retrieveUserDetails() {
        user = preferenceManager.retrieveUserDetails()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUserDetails()
        retirveVerificationCard()
    }

    private fun setUserDetails() {
        if(this::binding.isInitialized){
            binding.tvUsername.text = user.username
            binding.tvEmail.text = user.userId
            binding.tvDonorCount.text = user.donationCount.toString()
            binding.tvBloodGroup.text = user.bloodGroup.toString()
            if(user.latitude!=0.0 && user.longitude!=0.0){
                binding.tvAddress.text =
                    LocationUtils.getCurretntLocation(requireContext(),user.latitude,user.longitude)
            }else{
                binding.tvAddress.text = "N/A"
            }
            binding.tvLastDonatedDate.text = user.lastDonated.toString()
        }
    }

    private fun setFullScreenWindow() {
        requireActivity().window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if(hidden){
            requireActivity().window.decorView.systemUiVisibility =View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }else{
            retrieveUserDetails()
            setUserDetails()
            requireActivity().window.decorView.systemUiVisibility =0
        }
        super.onHiddenChanged(hidden)
    }
}