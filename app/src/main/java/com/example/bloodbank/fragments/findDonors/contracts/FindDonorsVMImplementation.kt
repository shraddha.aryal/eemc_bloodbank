package com.example.bloodbank.fragments.findDonors.contracts

interface FindDonorsVMImplementation {

    fun onSearchDonorClicked()
}