package com.example.bloodbank.fragments.settings.repository

import com.example.bloodbank.Utils.Constants
import com.example.bloodbank.Utils.PreferenceManager
import com.google.firebase.database.DatabaseReference
import javax.inject.Inject

class SettingsRepository @Inject constructor(
    private val databaseReference: DatabaseReference,
    private val preferenceManager: PreferenceManager
) {
    fun updateBecomeADonor(isDonor: Boolean) {
        val userDetails = preferenceManager.retrieveUserDetails()
        databaseReference.child(Constants.DONOR_LIST)
            .child(userDetails.bloodGroup!!).child(userDetails.userUniqueId!!)
            .setValue(userDetails)
        databaseReference.child("Registration").child(userDetails.contactNumber!!).child("donor").setValue(isDonor)
    }
}