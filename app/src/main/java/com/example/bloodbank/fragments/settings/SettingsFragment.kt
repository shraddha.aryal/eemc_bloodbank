package com.example.bloodbank.fragments.settings

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.bloodbank.R
import com.example.bloodbank.Utils.*
import com.example.bloodbank.databinding.FragmentSettingsBinding
import com.example.bloodbank.fragments.settings.viewModel.SettingsViewModel
import com.example.bloodbank.global.LocationVariables
import com.example.bloodbank.login.view.LoginView
import com.google.android.material.slider.Slider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_settings.*
import javax.inject.Inject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class SettingsFragment : Fragment() {
    private var searchButtonHeight: Float = 0f
    private lateinit var binding: FragmentSettingsBinding
    private val settingsViewModel: SettingsViewModel by viewModels()

    @Inject lateinit var firebaseAuth:FirebaseAuth

    @Inject
    lateinit var databaseReference: DatabaseReference

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private var searchDonorPeriphery: Double = LocationVariables.searchRadius
    private var searchRadiusMaxValue = 12000f

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        //binding.viewModel = findDonorsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSliderDefaultPosition()
        updateIsDonorStatus()
        setupHyperLink()
        retreveSearchConfigButtonHeight()
        searchRadiusMaxValue = binding.searchRadiusSlider.valueTo
        initListener()
    }

    private fun setupHyperLink() {
        binding.hyperLink.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initListener() {
        binding.becomeADonorSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isDonor ->
            if (isDonor) {
                //if last donor from is updated 30 days before then show form
                if (preferenceManager.retrieveLastDonationFormFilledDate()!!.isNotEmpty()) {
                    var checkLastDonorFormUpdatedDays = DateUtils.getDaysDifference(
                        preferenceManager.retrieveLastDonationFormFilledDate()
                    )
                    if (checkLastDonorFormUpdatedDays.toInt() < 30) {
                        var donorFormDao = preferenceManager.retrieveDonorFormDetails()
                        if (donorFormDao.age.isEmpty() || donorFormDao.diseases.isEmpty() || donorFormDao.medicine.isEmpty()
                            || donorFormDao.surgeries.isEmpty() || donorFormDao.weight.isEmpty()
                        ) {
                            binding.becomeADonorSwitch.isChecked = false
                            DonationForm.showDonationForm(requireActivity(), preferenceManager)
                        } else {
                            //If all form details are valid then valid the last donated date
                            if (donorFormDao.age.equals("18-60", true) &&
                                donorFormDao.weight.equals("Above 45 Kgs", true)
                                && donorFormDao.medicine.equals(
                                    "No",
                                    true
                                ) && donorFormDao.surgeries.equals(
                                    "No",
                                    true
                                ) && donorFormDao.diseases.equals("No", true)
                            ) {
                                //if last donated date is 90days before or more then proceed to become donor else show alert dialog
                                var lastDonatedDate =
                                    preferenceManager.retrieveUserDetails().lastDonated
                                if (!lastDonatedDate.equals("N/A", true)) {
                                    var checkDifferenceInDaysFromLastDonatedDate =
                                        DateUtils.getDaysDifference(lastDonatedDate)
                                    if (checkDifferenceInDaysFromLastDonatedDate.toDouble() > 90) {
                                        settingsViewModel.updateBecomeADonor(isDonor)
                                    } else {
                                        binding.becomeADonorSwitch.isChecked = false
                                        showAlertDialog()
                                    }
                                } else {
                                    settingsViewModel.updateBecomeADonor(isDonor)
                                }
                            } else {
                                binding.becomeADonorSwitch.isChecked = false
                                //if form data is not valid then show form
                                DonationForm.showDonationForm(
                                    requireActivity(),
                                    preferenceManager
                                )
                            }
                        }
                    } else {
                        binding.becomeADonorSwitch.isChecked = false
                        DonationForm.showDonationForm(requireActivity(), preferenceManager)
                    }
                } else {
                    binding.becomeADonorSwitch.isChecked = false
                    DonationForm.showDonationForm(requireActivity(), preferenceManager)
                }

            }

        })

        binding.searchRadiusSlider.addOnChangeListener(Slider.OnChangeListener { slider, value, fromUser ->
            searchDonorPeriphery = value.toDouble()
            binding.etSearchPeriphery.setText(
                InputValidation.twoDigitDecimalRoundOff((value / 1000).toDouble()).toString()
            )
            LocationVariables.searchRadius = searchDonorPeriphery
        })


        binding.etSearchPeriphery.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (binding.etSearchPeriphery.isFocused) {
                    if (s.isNotEmpty()) {
                        searchDonorPeriphery = s.toString().toDouble() * 1000
                        showSearchConfigButtonVisibility()
                    } else {
                        hideSearchConfigLayout()
                    }
                }
            }
        })

        binding.llSignOut.setOnClickListener {
            firebaseAuth.signOut()
            startActivity(Intent(requireActivity(), LoginView::class.java))
            requireActivity().finishAffinity()

        }


        btn_apply.setOnClickListener {
            if (binding.etSearchPeriphery.text.toString()
                    .toFloat() * 1000 > LocationVariables.searchRadiusMaxValue
            ) {
                binding.etSearchPeriphery.setText((LocationVariables.searchRadiusMaxValue / 1000).toString())
                binding.etSearchPeriphery.clearFocus()
            } else {
                updateSearchPeriphery()
            }

        }
        btn_cancel.setOnClickListener {
            hideSearchConfigLayout()
        }
        binding.ivSearchPeripherySetting.setOnClickListener {
            showSettingDialog()
        }
    }

    private fun showSettingDialog() {
        var searchRadiusMaxCountSettingDialog: Dialog = Dialog(requireContext())
        searchRadiusMaxCountSettingDialog.setContentView(R.layout.dialog_search_radius_max_distance_setting)

        var et_radius = searchRadiusMaxCountSettingDialog.findViewById<EditText>(R.id.et_radius)
        var btn_apply = searchRadiusMaxCountSettingDialog.findViewById<Button>(R.id.btn_apply)
        var btn_cancel = searchRadiusMaxCountSettingDialog.findViewById<Button>(R.id.btn_cancel)
        et_radius.setText((LocationVariables.searchRadiusMaxValue / 1000).toString())
        btn_apply.setOnClickListener {
            var inputRadius =
                et_radius.text.toString().toDouble() * 1000   //input radius in meter
            if (inputRadius < LocationVariables.searchRadiusMaxValue) {
                //set max value of slider to input radius value
                binding.searchRadiusSlider.value = inputRadius.toFloat()    //update slider value
                binding.searchRadiusSlider.valueTo =
                    inputRadius.toFloat()  // update the slider max value
                LocationVariables.searchRadiusMaxValue =
                    inputRadius        //update global search radius max value
            } else {
                LocationVariables.searchRadiusMaxValue = inputRadius
                binding.searchRadiusSlider.valueTo = inputRadius.toFloat()
            }
            searchRadiusMaxCountSettingDialog.dismiss()

        }

        btn_cancel.setOnClickListener {
            searchRadiusMaxCountSettingDialog.dismiss()
        }


        val dialogWindow = searchRadiusMaxCountSettingDialog.window
        val m: WindowManager = requireActivity().windowManager
        val d = m.defaultDisplay
        assert(dialogWindow != null)
        dialogWindow!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = dialogWindow!!.attributes
        lp.width = (d.width * 0.9).toInt()
        searchRadiusMaxCountSettingDialog.setCancelable(false)
        searchRadiusMaxCountSettingDialog.show()
    }

    private fun showAlertDialog() {

        var alertDialog: Dialog = Dialog(requireContext())
        alertDialog.setContentView(R.layout.alert_dialog_become_a_donor)


        var btn_ok = alertDialog.findViewById<Button>(R.id.btn_ok)
        btn_ok.setOnClickListener {
            alertDialog.dismiss()

        }
        alertDialog.setCancelable(false)
        val dialogWindow = alertDialog.window
        val m: WindowManager = requireActivity().windowManager
        val d = m.defaultDisplay
        assert(dialogWindow != null)
        dialogWindow!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = dialogWindow!!.attributes
        lp.width = (d.width * 0.9).toInt()
        alertDialog.show()
    }

    private fun setSliderDefaultPosition() {
        binding.searchRadiusSlider.value = LocationVariables.searchRadius.toFloat()
        binding.etSearchPeriphery.setText(
            InputValidation.twoDigitDecimalRoundOff(LocationVariables.searchRadius / 1000)
                .toString()
        )
    }

    private fun retreveSearchConfigButtonHeight() {
        searchButtonHeight = ll_search_config_buttons.width.toFloat()
    }

    private fun showSearchConfigButtonVisibility() {
        binding.llSearchConfigButtons.visibility = View.VISIBLE
        binding.llSearchConfigButtons.animate().translationY(searchButtonHeight)

    }

    private fun updateSearchPeriphery() {
        searchDonorPeriphery = binding.etSearchPeriphery.text.toString().toDouble() * 1000
        binding.searchRadiusSlider.value = searchDonorPeriphery.toFloat()
        binding.etSearchPeriphery.clearFocus()
        hideSearchConfigLayout()
    }

    private fun hideSearchConfigLayout() {
        binding.llSearchConfigButtons.visibility = View.GONE
        binding.llSearchConfigButtons.animate().translationY(searchButtonHeight)
    }

    private fun updateIsDonorStatus() {
        val userDetails = preferenceManager.retrieveUserDetails()
        if (this::binding.isInitialized) {
            binding.becomeADonorSwitch.isChecked = userDetails.donor
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) {
            updateIsDonorStatus()
        }
    }


}