package com.example.bloodbank.fragments.settings.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.example.bloodbank.fragments.settings.repository.SettingsRepository

class SettingsViewModel @ViewModelInject constructor(private val settingsRepository: SettingsRepository) :
    ViewModel() {
    fun updateBecomeADonor(isDonor: Boolean) {
        settingsRepository.updateBecomeADonor(isDonor)
    }


}