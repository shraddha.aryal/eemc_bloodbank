package com.example.bloodbank.fragments.findDonors.viewModel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.example.bloodbank.fragments.findDonors.contracts.FindDonorsVMImplementation
import com.example.bloodbank.fragments.findDonors.repostiory.FindDonorsReposiotry


class FindDonorsViewModel @ViewModelInject constructor(private val findDonorsReposiotry: FindDonorsReposiotry) :
    ViewModel(), FindDonorsVMImplementation {
    private var isSearchDonorClicked:MutableLiveData<Boolean> = MutableLiveData(false)

    var selectedTabText: String = ""
    var spinnerSelectedLocation: String = ""

    override fun onSearchDonorClicked() {
            isSearchDonorClicked.postValue(true)
    }


    fun fetchIsSearchDonorClicked():MutableLiveData<Boolean> = isSearchDonorClicked

}