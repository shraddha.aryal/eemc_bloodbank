package com.example.bloodbank.fragments.findDonors.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.example.bloodbank.R
import com.example.bloodbank.Utils.Constants

import com.example.bloodbank.databinding.FragmentFindDonorsBinding
import com.example.bloodbank.donerList.view.DonorsView
import com.example.bloodbank.fragments.findDonors.viewModel.FindDonorsViewModel
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint


/**
 * A simple [Fragment] subclass.
 * Use the [FindDonorsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class FindDonorsFragment : Fragment() {
    private val findDonorsViewModel: FindDonorsViewModel by viewModels()
    private lateinit var binding: FragmentFindDonorsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFindDonorsBinding.inflate(inflater, container, false)
        binding.viewModel = findDonorsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewModelsData()
        initListener()
        initObserver()
    }

    private fun initObserver() {
        findDonorsViewModel.fetchIsSearchDonorClicked().observe(requireActivity()) {
            if (it) {
                navigateToDonorsListActivity()
            }
        }
    }

    private fun navigateToDonorsListActivity() {
        val intent = Intent(activity, DonorsView::class.java)
        intent.putExtra(Constants.LOCATION, findDonorsViewModel.spinnerSelectedLocation)
        intent.putExtra(Constants.BLOOD_GROUP,findDonorsViewModel.selectedTabText)
        startActivity(intent)
        requireActivity()!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left) // adding animation
    }


    private fun setViewModelsData() {
        findDonorsViewModel.selectedTabText = binding.tabLayout1.getTabAt(0)?.text.toString()
    }

    private fun initListener() {
        binding.tabLayout1.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                findDonorsViewModel.selectedTabText = tab?.text.toString()
            }
        })

    }


}