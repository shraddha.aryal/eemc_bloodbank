package com.example.bloodbank.registration.repo

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.example.bloodbank.Utils.*
import com.example.bloodbank.donerList.data.User
import com.google.android.gms.auth.api.signin.internal.Storage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import javax.inject.Inject


class RegistrationRepository @Inject constructor(
    private val mAuth: FirebaseAuth,
    private val databaseReference: DatabaseReference,
    private val preferenceManager: PreferenceManager,
    private val storageReference: StorageReference
) {
    var userRegistrationResponse: MutableLiveData<Resource<String>> = MutableLiveData()

    fun registerUser(
        username: String,
        contactNumber: String,
        userId: String,
        password: String,
        bloodGroup: String,
        location: String,
        observableImagePath: Uri
    ) {
        userRegistrationResponse.postValue(Resource(Status.LOADING, null, null))
        mAuth.createUserWithEmailAndPassword(userId, password).addOnCompleteListener {
            if (it.isSuccessful) {
                userRegistrationResponse.postValue(
                    Resource(
                        Status.SUCCESS,
                        null,
                        "User Successfully Registered"
                    )
                )
                val userUniqueId = UniqueIDGenerator.getUniqueId()
                val user = User(
                    bloodGroup = bloodGroup,
                    location = location,
                    userId = userId,
                    username = username,
                    contactNumber = contactNumber,
                    userUniqueId = userUniqueId
                )
                saveRegisteredUser(user)
                databaseReference.child("Registration").child(contactNumber).setValue(user)

               var verificationImageRef = storageReference.child(contactNumber)
                verificationImageRef.putFile(observableImagePath)

            } else {
                userRegistrationResponse.postValue(
                    Resource(
                        Status.ERROR,
                        null,
                        it.exception.toString()
                    )
                )
            }
        }
    }


    fun saveRegisteredUser(user: User) {
        preferenceManager.saveUserDetails(user)
     //   databaseReference.child(Constants.REGISTERED_USER).child(userDetails.userUniqueId).setValue(userDetails)

    }
}