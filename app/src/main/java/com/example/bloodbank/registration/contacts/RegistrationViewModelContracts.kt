package com.example.bloodbank.registration.contacts

interface RegistrationViewModelContracts {
    fun onContinueClicked()
    fun onBackPressed()
}