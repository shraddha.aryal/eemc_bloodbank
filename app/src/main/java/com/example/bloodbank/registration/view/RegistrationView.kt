package com.example.bloodbank.registration.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import com.example.bloodbank.R
import com.example.bloodbank.Utils.Constants
import com.example.bloodbank.Utils.SnackBar
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.databinding.ActivityRegistrationViewBinding
import com.example.bloodbank.registration.viewModel.RegistrationViewModel
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException
import javax.inject.Inject


@AndroidEntryPoint
class RegistrationView : AppCompatActivity() {
    private lateinit var filePath: Uri
    private val PICK_IMAGE_REQUEST: Int = 107

    @Inject
    lateinit var mAuth: FirebaseAuth
    lateinit var binding: ActivityRegistrationViewBinding
    private val registrationViewModel: RegistrationViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Binding the view with xml
        binding =
            ActivityRegistrationViewBinding.inflate(layoutInflater)
        binding.viewModel = registrationViewModel
        setContentView(binding.root)

        initListener()
        initObserver()
        setViewModelsData()
    }

    private fun setViewModelsData() {
        registrationViewModel.selectedTabText = binding.tabLayout1.getTabAt(0)?.text.toString()

    }

    private fun initListener() {
        binding.tabLayout1.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                registrationViewModel.selectedTabText = tab?.text.toString()
            }
        })


        binding.ivChooseImage.setOnClickListener {
            chooseImage()
        }

    }

    private fun chooseImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }



    private fun initObserver() {
        registrationViewModel.checkUserIdIsEmpty.observe(this) {
            if (it) binding.etUserId.error = getString(R.string.userid_empty)
        }
        registrationViewModel.checkUserPasswordIsEmpty.observe(this) {
            if (it) binding.etPassword.error = getString(R.string.password_empty)
        }
        registrationViewModel.checkIfPasswordAndConfirmPasswordMatches.observe(this) {
            if (!it) binding.etConfirmPassword.error = getString(R.string.password_not_match)
        }
        registrationViewModel.onBackPressedObserver.observe(this) {
            if (it) {
                closeRegistrationActivity()
            }
        }
        registrationViewModel.fetchRegisterUserResponse().observe(this) {
            it?.let { resource ->
                when (resource.status) {
                    //if status is successful
                    Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        SnackBar.showSnackBar(
                            it.message.toString(),
                            binding.coordinatorLayoutRegistration,
                            getColor(R.color.successColor),
                            getColor(R.color.whiteColor)
                        )
                    }
                    //if status is error
                    Status.ERROR -> {
                        binding.progressBar.visibility = View.GONE
                        SnackBar.showSnackBar(
                            it.message.toString(),
                            binding.coordinatorLayoutRegistration
                        )
                    }
                    //if status is loading
                    Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                }
            }
        }
    }


    private fun closeRegistrationActivity() {
        this.finish()       //closing the activity
        overridePendingTransition(
            R.anim.slide_in_left, R.anim.slide_out_right
        )
    }

    override fun onBackPressed() {
        closeRegistrationActivity()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            filePath = data.data!!
            try {
                registrationViewModel.observableImagePath= filePath!!
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                binding.ivChooseImage.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}


