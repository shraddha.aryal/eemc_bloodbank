package com.example.bloodbank.registration.viewModel

import android.net.Uri
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bloodbank.Utils.InputValidation
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.registration.contacts.RegistrationViewModelContracts
import com.example.bloodbank.registration.repo.RegistrationRepository

class RegistrationViewModel @ViewModelInject constructor(private val registrationRepository: RegistrationRepository) :
    ViewModel()
    , RegistrationViewModelContracts {
    /**
     * Observable field that observe the input text in ui
     * */
    var observableUsername:String =""
    var observableContactNumber: String = ""
    var observableUserId: String = ""
    var observablePassword: String = ""
    var observableConfirmPassword = ""
    var selectedTabText: String = ""
    var spinnerSelectedLocation: String = ""
    var observableImagePath: Uri? = null

    var checkUserIdIsEmpty: MutableLiveData<Boolean> = MutableLiveData(false)
    var checkUserPasswordIsEmpty: MutableLiveData<Boolean> = MutableLiveData(false)
    var checkIfPasswordAndConfirmPasswordMatches: MutableLiveData<Boolean> = MutableLiveData(true)
    var onBackPressedObserver: MutableLiveData<Boolean> = MutableLiveData(false)

    private var registerUserResponse: MutableLiveData<Resource<String>> =
        registrationRepository.userRegistrationResponse

    fun registerUser(
        username:String,
        contactNumber:String,
        userId: String,
        password: String,
        confirmPassword: String,
        bloodGroup: String,
        location: String
    ) {
        var checkIsEmpty =
            InputValidation.checkIsEmpty(userId, password)


        if (checkIsEmpty) {
            checkUserIdIsEmpty.postValue(InputValidation.checkIsInputEmpty(userId))
            checkUserPasswordIsEmpty.postValue(InputValidation.checkIsInputEmpty(password))
        } else {
            if(observableImagePath!=null){
                var isMatchingPassword =
                    InputValidation.checkIfPasswordAndConfirmPasswordMatches(password, confirmPassword)
                if (!isMatchingPassword) {
                    checkIfPasswordAndConfirmPasswordMatches.postValue(false)
                } else {
                    registrationRepository.registerUser(username,contactNumber,userId, password,bloodGroup,location,observableImagePath!!)
                }
            }else{
                //TODO
            }

        }
    }

    override fun onContinueClicked() {
        registerUser(observableUsername,observableContactNumber,observableUserId, observablePassword, observableConfirmPassword,selectedTabText,spinnerSelectedLocation)
    }

    override fun onBackPressed() {
        if (onBackPressedObserver.value == false) {
            onBackPressedObserver.postValue(true)
        }
    }

    fun fetchRegisterUserResponse(): LiveData<Resource<String>> = registerUserResponse
}