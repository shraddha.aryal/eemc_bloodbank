package com.example.bloodbank.registration.dao

data class UserDetails(
    val user_name: String = "",

    val contact_number: String,

    val user_id: String,

    val blood_group: String,

    val location: String,

    val user_unique_id: String,

    val is_donor: Boolean = false
)