package com.example.bloodbank.verification.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bloodbank.Utils.InputValidation
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.verification.contracts.VerificationViewModelContracts
import com.example.bloodbank.verification.repository.VerificationRepository
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider

class VerificationViewModel @ViewModelInject constructor(private val verificationRepository: VerificationRepository): ViewModel(),VerificationViewModelContracts {
    var code1:String= ""
    var code2:String= ""
    var code3:String= ""
    var code4:String= ""
    var code5:String= ""
    var code6:String = ""
    var totalCode:String = ""
    var code1EmptyObserver:MutableLiveData<Boolean> = MutableLiveData(false)
    var code2EmptyObserver:MutableLiveData<Boolean> = MutableLiveData(false)
    var code3EmptyObserver:MutableLiveData<Boolean> = MutableLiveData(false)
    var code4EmptyObserver:MutableLiveData<Boolean> = MutableLiveData(false)
    var code5EmptyObserver:MutableLiveData<Boolean> = MutableLiveData(false)
    var code6EmptyObserver:MutableLiveData<Boolean> = MutableLiveData(false)
    var code1TextChangeObserver:MutableLiveData<Boolean> = MutableLiveData()
    var code2TextChangeObserver:MutableLiveData<Boolean> = MutableLiveData()
    var code3TextChangeObserver:MutableLiveData<Boolean> = MutableLiveData()
    var code4TextChangeObserver:MutableLiveData<Boolean> = MutableLiveData()
    var code5TextChangeObserver:MutableLiveData<Boolean> = MutableLiveData()
    var code6TextChangeObserver:MutableLiveData<Boolean> = MutableLiveData()
    var signInWithPhoneNumberResponse :MutableLiveData<Resource<FirebaseUser>> = verificationRepository.signInWithPhoneNumberResponse
    var isCodeFieldEmpty :MutableLiveData<Boolean> = MutableLiveData()

    override fun onContinueClick() {
        if(code1.isNotEmpty()&&code2.isNotEmpty()&&code3.isNotEmpty()&&code4.isNotEmpty()&&code5.isNotEmpty()&&code6.isNotEmpty()){
            totalCode = code1+code2+code3+code4+code5+code6
            isCodeFieldEmpty.postValue(false)
        }else{
            if(InputValidation.checkIsInputEmpty(code1)) code1EmptyObserver.postValue(true)
            if(InputValidation.checkIsInputEmpty(code2)) code2EmptyObserver.postValue(true)
            if(InputValidation.checkIsInputEmpty(code3)) code3EmptyObserver.postValue(true)
            if(InputValidation.checkIsInputEmpty(code4)) code4EmptyObserver.postValue(true)
            if(InputValidation.checkIsInputEmpty(code5)) code5EmptyObserver.postValue(true)
            if(InputValidation.checkIsInputEmpty(code6)) code6EmptyObserver.postValue(true)
        }
    }

    override fun onCode1TextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if(s.length==1) code1TextChangeObserver.postValue(true) else code1TextChangeObserver.postValue(false)
    }

    override fun onCode2TextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if(s.length==1) code2TextChangeObserver.postValue(true) else code2TextChangeObserver.postValue(false)
    }

    override fun onCode3TextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if(s.length==1) code3TextChangeObserver.postValue(true) else code3TextChangeObserver.postValue(false)
    }

    override fun onCode4TextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if(s.length==1) code4TextChangeObserver.postValue(true) else code4TextChangeObserver.postValue(false)
    }

    override fun onCode5TextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if(s.length==1) code5TextChangeObserver.postValue(true) else code5TextChangeObserver.postValue(false)
    }

    override fun onCode6TextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if(s.length==1) code6TextChangeObserver.postValue(true) else code6TextChangeObserver.postValue(false)
    }

    override fun signInWithPhoneNumber(verificationID: String) {
        val credential = PhoneAuthProvider.getCredential(verificationID,totalCode)
        verificationRepository.signInWithPhoneNumber(credential)
    }

    fun fetchSignInWithPhoneNumberResponse(): LiveData<Resource<FirebaseUser>> = signInWithPhoneNumberResponse


}