package com.example.bloodbank.verification.contracts

import com.google.firebase.auth.PhoneAuthCredential

interface VerificationViewModelContracts {

    fun onContinueClick()

    fun onCode1TextChanged(
        s: CharSequence, start: Int, before: Int,
        count: Int
    )

    fun onCode2TextChanged(
        s: CharSequence, start: Int, before: Int,
        count: Int
    )


    fun onCode3TextChanged(
        s: CharSequence, start: Int, before: Int,
        count: Int
    )

    fun onCode4TextChanged(
        s: CharSequence, start: Int, before: Int,
        count: Int
    )

    fun onCode5TextChanged(
        s: CharSequence, start: Int, before: Int,
        count: Int
    )

    fun onCode6TextChanged(
        s: CharSequence, start: Int, before: Int,
        count: Int
    )

    fun signInWithPhoneNumber(verificationID: String)
}