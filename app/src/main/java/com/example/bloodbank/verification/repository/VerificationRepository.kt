package com.example.bloodbank.verification.repository

import androidx.lifecycle.MutableLiveData
import com.example.bloodbank.Utils.Resource
import com.example.bloodbank.Utils.Status
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.UserInfo

import javax.inject.Inject

class VerificationRepository @Inject constructor(private val mAuth: FirebaseAuth) {
     var signInWithPhoneNumberResponse:MutableLiveData<Resource<FirebaseUser>> = MutableLiveData()

    fun signInWithPhoneNumber(credential: PhoneAuthCredential) {
        signInWithPhoneNumberResponse.postValue(Resource(Status.LOADING,null,null))
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user:FirebaseUser = task.result!!.user
                    signInWithPhoneNumberResponse.postValue(Resource(Status.SUCCESS,user,"User Signed in Successfully"))

                } else {
                    signInWithPhoneNumberResponse.postValue(Resource(Status.ERROR,null,task.exception!!.localizedMessage.toString()))
                }
            }

    }
}