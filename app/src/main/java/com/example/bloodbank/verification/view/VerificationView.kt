package com.example.bloodbank.verification.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import com.example.bloodbank.MainActivity
import com.example.bloodbank.R
import com.example.bloodbank.Utils.Constants.PHONE_NUMBER
import com.example.bloodbank.Utils.SnackBar
import com.example.bloodbank.Utils.Status
import com.example.bloodbank.databinding.ActivityVerificationViewBinding
import com.example.bloodbank.verification.viewModel.VerificationViewModel
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class VerificationView : AppCompatActivity() {
    private lateinit var globalVerificationId: String
    private val verificationViewModel: VerificationViewModel by viewModels()

    @Inject
    lateinit var mAuth: FirebaseAuth
    lateinit var binding: ActivityVerificationViewBinding
    lateinit var number: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVerificationViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewModel = verificationViewModel
        number = intent.getStringExtra(PHONE_NUMBER).toString()
        binding.tvInfo.text = binding.tvInfo.text.toString() + " $number"
        binding.progressBar.visibility = View.VISIBLE
        initObserver()
        initTextWatcher()
        initCallBacks()
        verifyNumber(number)

    }

    private fun initTextWatcher() {

    }

    private fun initObserver() {
        //----------------Check if the code is empty after pressing the continue button-------------------//
        verificationViewModel.code1EmptyObserver.observe(this) {
            if (it) binding.etCode1.setBackgroundResource(R.drawable.verification_code_error_background)
        }
        verificationViewModel.code2EmptyObserver.observe(this) {
            if (it) binding.etCode2.setBackgroundResource(R.drawable.verification_code_error_background)
        }
        verificationViewModel.code3EmptyObserver.observe(this) {
            if (it) binding.etCode3.setBackgroundResource(R.drawable.verification_code_error_background)
        }
        verificationViewModel.code4EmptyObserver.observe(this) {
            if (it) binding.etCode4.setBackgroundResource(R.drawable.verification_code_error_background)
        }
        verificationViewModel.code5EmptyObserver.observe(this) {
            if (it) binding.etCode5.setBackgroundResource(R.drawable.verification_code_error_background)
        }
        verificationViewModel.code6EmptyObserver.observe(this) {
            if (it) binding.etCode6.setBackgroundResource(R.drawable.verification_code_error_background)
        }

        //-------------------Changing focus if the user input the text in code field------------------------//
        verificationViewModel.code1TextChangeObserver.observe(this) {
            if (it) binding.etCode2.requestFocus() else binding.etCode1.clearFocus()
        }
        verificationViewModel.code2TextChangeObserver.observe(this) {
            if (it) binding.etCode3.requestFocus() else binding.etCode1.requestFocus()
        }
        verificationViewModel.code3TextChangeObserver.observe(this) {
            if (it) binding.etCode4.requestFocus() else binding.etCode2.requestFocus()
        }
        verificationViewModel.code4TextChangeObserver.observe(this) {
            if (it) binding.etCode5.requestFocus() else binding.etCode3.requestFocus()
        }
        verificationViewModel.code5TextChangeObserver.observe(this) {
            if (it) binding.etCode6.requestFocus() else binding.etCode4.requestFocus()
        }
        verificationViewModel.code6TextChangeObserver.observe(this) {
            if (it) binding.etCode6.clearFocus() else binding.etCode5.requestFocus()
        }

        verificationViewModel.fetchSignInWithPhoneNumberResponse().observe(this) {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        navigateToMain()
                    }
                    Status.ERROR -> {
                        binding.progressBar.visibility = View.GONE
                        SnackBar.showSnackBar(
                            it.message.toString(),
                            binding.coordinatorLayoutVerification,
                            getColor(R.color.errorColor),
                            getColor(R.color.subHeaderColor)
                        )
                    }
                }
            }
        }

        verificationViewModel.isCodeFieldEmpty.observe(this) {
            if (!it) {
                verificationViewModel.signInWithPhoneNumber(globalVerificationId)
            }
        }


    }

    private fun navigateToMain() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    fun verifyNumber(phoneNumber: String) {
        val options = PhoneAuthOptions.newBuilder(mAuth)
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }


    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    private fun initCallBacks() {
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {

                Log.d(TAG, "onVerificationCompleted:$credential")
            }

            override fun onVerificationFailed(e: FirebaseException) {

                if (e is FirebaseAuthInvalidCredentialsException) {
                    //Invalid parameter
                    Log.e(TAG, e.localizedMessage.toString())
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    Log.e(TAG, e.localizedMessage.toString())
                }

            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                Log.d(TAG, "onCodeSent:$verificationId")
                globalVerificationId = verificationId
                binding.etCode1.isEnabled = true
                binding.etCode2.isEnabled = true
                binding.etCode3.isEnabled = true
                binding.etCode4.isEnabled = true
                binding.etCode5.isEnabled = true
                binding.etCode6.isEnabled = true
                binding.progressBar.visibility = View.GONE

            }
        }
    }

    companion object {
        val TAG = "LoginView "
    }
}