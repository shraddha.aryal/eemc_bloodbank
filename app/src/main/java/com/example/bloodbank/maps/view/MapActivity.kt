package com.example.bloodbank.maps.view

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.example.bloodbank.R
import com.example.bloodbank.Utils.ImageConverter
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.dashboard.DashboardActivity
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.global.LocationVariables
import com.example.bloodbank.maps.enum.MapEnum
import com.example.bloodbank.maps.viewModel.MapViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
open class MapActivity : AppCompatActivity(), OnMapReadyCallback {
    private var userList: ArrayList<User>? = null
    private var isUserList: Boolean = false
    private lateinit var mCircle: Circle
    private lateinit var myLocation: LatLng
    private lateinit var requestedLocation: LatLng
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private var tvMyLocationWidth = 0f
    private var tvRequestedLocationWidth = 0f
    private lateinit var user: User
    private lateinit var mMap: GoogleMap
    private lateinit var tvUsername: TextView
    private lateinit var tvLocation: TextView
    private lateinit var tvBloodGroup: TextView
    private lateinit var tvTitle: TextView
    private lateinit var tvContact: TextView
    private lateinit var cvMyLocation: CardView
    private lateinit var cvRequestedLocation: CardView
    private lateinit var tvMyLocation: TextView
    private lateinit var tvRequestedLocation: TextView
    private lateinit var btnMakeACall: Button
    private lateinit var btnCancel: Button
    private lateinit var btnAccept: Button
    private lateinit var bottomeSheetLayout: LinearLayout
    private val viewModel: MapViewModel by viewModels()

    @Inject
    lateinit var preferenceManager: PreferenceManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFullScreenWindow()
        getMyLocation()
        retrieveIntentData()
        setContentView(R.layout.activity_map)
        setupBinding()
        setupBindingData()
        initListener()
        // Get the SupportMapFragment and request notification when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    private fun getMyLocation() {
        // Add a marker in Sydney and move the camera
        myLocation = LatLng(LocationVariables.latitude, LocationVariables.longitude)
    }

    private fun initListener() {
        tvTitle.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                hideBottomSheet()
            } else {
                showBottomSheet()
            }
        }

        cvMyLocation.setOnLongClickListener {
            alterTheTextVisibilityOnMyLocationCard()
            true
        }
        cvRequestedLocation.setOnLongClickListener {
            alterTheTextVisibilityOnRequestedLocationCard()
            true
        }
        cvRequestedLocation.setOnClickListener {
            if (this::mMap.isInitialized) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(requestedLocation, 15f))
            }
        }
        cvMyLocation.setOnClickListener {
            if (this::mMap.isInitialized) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))
            }
        }

        btnMakeACall.setOnClickListener {
            openDialer()
        }

    }

    private fun openDialer() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:${user.contactNumber}}")
        startActivity(intent)
    }

    private fun alterTheTextVisibilityOnRequestedLocationCard() {
        if (tvRequestedLocation.visibility == View.VISIBLE) tvRequestedLocation.visibility =
            View.GONE else tvRequestedLocation.visibility = View.VISIBLE
        tvRequestedLocation.animate().translationX(tvRequestedLocationWidth)
    }

    private fun alterTheTextVisibilityOnMyLocationCard() {
        if (tvMyLocation.visibility == View.VISIBLE) tvMyLocation.visibility =
            View.GONE else tvMyLocation.visibility = View.VISIBLE
        tvMyLocation.animate().translationX(tvMyLocationWidth)
    }

    private fun showBottomSheet() {
        tvTitle.text = "Hide User Details"
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun hideBottomSheet() {
        tvTitle.text = "Show User Details"
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun setupBindingData() {
        tvUsername.text = user.username
        tvLocation.text = user.location
        tvBloodGroup.text = user.bloodGroup
        tvContact.text = user.contactNumber
        //btnMakeACall.text = "Call (${user.contactNumber})"
    }

    private fun setupBinding() {
        bottomeSheetLayout = findViewById(R.id.layoutBottomSheet)
        tvUsername = bottomeSheetLayout.findViewById(R.id.tv_username)
        tvLocation = bottomeSheetLayout.findViewById(R.id.tv_location)
        tvBloodGroup = bottomeSheetLayout.findViewById(R.id.tv_bloodGroup)
        tvTitle = bottomeSheetLayout.findViewById(R.id.tv_title)
        tvContact = bottomeSheetLayout.findViewById(R.id.tv_contactNumber)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomeSheetLayout)
        btnMakeACall = bottomeSheetLayout.findViewById(R.id.btnMakeAcall)
        btnAccept = bottomeSheetLayout.findViewById(R.id.btn_accept)
        btnCancel = bottomeSheetLayout.findViewById(R.id.btn_cancel)

        cvMyLocation = findViewById(R.id.cv_my_location)
        cvRequestedLocation = findViewById(R.id.cv_requested_location)
        tvMyLocation = findViewById(R.id.tv_my_location)
        tvRequestedLocation = findViewById(R.id.tv_requested_location)

        tvMyLocationWidth = tvMyLocation.width.toFloat()
        tvRequestedLocationWidth = tvRequestedLocation.width.toFloat()
        btnCancel.setOnClickListener {
            viewModel.cancelRequest(user)
        }

        btnAccept.setOnClickListener {
            btnAccept.isEnabled = false
            btnAccept.visibility = View.GONE
            viewModel.acceptBloodDonationRequest(user)
        }

    }


    private fun retrieveIntentData() {
        val bundle = intent.getBundleExtra("bundle")
        isUserList = intent.getBooleanExtra("isUserList", false)
        if (isUserList) {
            userList = intent.getParcelableArrayListExtra<User>(MapEnum.UserList.value)
        } else {
            user = bundle!!.getSerializable(MapEnum.User.value)!! as User
            requestedLocation = LatLng(user.latitude, user.longitude)
        }

    }

    private fun setFullScreenWindow() {
        window?.decorView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        window.statusBarColor = Color.TRANSPARENT
        window.navigationBarColor = Color.WHITE
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnCameraMoveListener {
            hideBottomSheet()
        }
        addMarker(requestedLocation, true, R.drawable.ic_map_marker)
        addMarker(myLocation, true, R.drawable.ic_my_location_map_marker)
        addRadiusInMap()
        val center = CameraUpdateFactory.newLatLng(
            requestedLocation
        )
        val zoom = CameraUpdateFactory.zoomTo(13f)
        mMap.moveCamera(center)
        mMap.animateCamera(zoom)


        /*    val path: MutableList<List<LatLng>> = ArrayList()
            val directionsRequest = object : StringRequest(Request.Method.GET, Location.getDirection(
                LocationDAO(  27.661489044164032,85.3194218701495), LocationDAO(latitude,longitude)
            ), Response.Listener<String> {
                    response ->
                val jsonResponse = JSONObject(response)
                // Get routes
                val routes = jsonResponse.getJSONArray("routes")
                val legs = routes.getJSONObject(0).getJSONArray("legs")
                val steps = legs.getJSONObject(0).getJSONArray("steps")
                for (i in 0 until steps.length()) {
                    val points = steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                    path.add(PolyUtil.decode(points))
                }
                for (i in 0 until path.size) {
                    this.mMap!!.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
                }
            }, Response.ErrorListener {
                    _ ->
            }){}
            val requestQueue = Volley.newRequestQueue(this)
            requestQueue.add(directionsRequest)*/
    }

    private fun addRadiusInMap() {
        val radiusInMeters = 2000.0
        val strokeColor = -0x10000 //red outline
        val shadeColor = 0x44ff0000 //opaque red fill
        val circleOptions =
            CircleOptions().center(requestedLocation).radius(radiusInMeters).fillColor(shadeColor)
                .strokeColor(strokeColor).strokeWidth(4f)
        mCircle = mMap.addCircle(circleOptions)
    }


    fun addMarker(position: LatLng?, draggable: Boolean, iconResId: Int) {
        val markerOptions = MarkerOptions()
        markerOptions.draggable(draggable)
        markerOptions.icon(ImageConverter.BitmapFromVector(this, iconResId))
        markerOptions.position(position)
        mMap.addMarker(markerOptions)
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            hideBottomSheet()
        } else {
            navigateToDashboard()
        }
    }

    private fun navigateToDashboard() {
        startActivity(Intent(this, DashboardActivity::class.java))
    }
}
