package com.example.bloodbank.maps.repo

import com.example.bloodbank.Utils.Constants
import com.example.bloodbank.Utils.DateUtils
import com.example.bloodbank.Utils.PreferenceManager
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.global.LocationVariables
import com.google.firebase.database.DatabaseReference
import javax.inject.Inject

class MapRepository @Inject constructor(
    private val databaseReference: DatabaseReference,
    private val preferenceManager: PreferenceManager
) {

    fun cancelRequest(user: User) {

        databaseReference.child("Cancelled").child(user.userUniqueId!!).setValue(user)
        databaseReference.child(Constants.REQUEST).child(preferenceManager.retrieveUniqueId()!!).removeValue()

    }

    fun acceptRequest(user: User) {

        user.latitude = LocationVariables.latitude
        user.longitude = LocationVariables.longitude
        databaseReference.child("Accepted").child(user.userUniqueId!!).setValue(user)
        databaseReference.child("Registration").child(preferenceManager.retrieveUserDetails().contactNumber!!).child("lastDonated").setValue(DateUtils.getCurrentDate())
        preferenceManager.updateLastDonatedDate(DateUtils.getCurrentDate())
        preferenceManager.saveUserDetails(user)
        databaseReference.child("DonorList").child(preferenceManager.retrieveUserDetails().bloodGroup!!).child(preferenceManager.retrieveUniqueId()!!).removeValue()

    }
}