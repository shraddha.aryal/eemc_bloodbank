package com.example.bloodbank.maps.enum

enum class MapEnum(var value:String) {
    User("User"),
    UserList("userList")
}