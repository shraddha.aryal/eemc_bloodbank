package com.example.bloodbank.maps.viewModel

import android.location.Location
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.maps.repo.MapRepository

class MapViewModel @ViewModelInject constructor(private val mapRepository: MapRepository) :
    ViewModel() {
    fun cancelRequest(user: User) {
        mapRepository.cancelRequest(user)
    }

    fun acceptBloodDonationRequest(user: User) {
        mapRepository.acceptRequest(user)
    }
}