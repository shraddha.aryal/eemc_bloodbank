package com.example.bloodbank.di

import android.app.Application
import com.example.bloodbank.location.LocationUtils
import dagger.hilt.android.HiltAndroidApp
/**
 * @HiltAndroidApp triggers Hilt's code generation,
 * including a base class for your application that serves as the application-level dependency container.
 * */
@HiltAndroidApp
class BaseApplication : Application(){}
