package com.example.bloodbank.Utils

import java.util.*

object UniqueIDGenerator{

    fun getUniqueId():String{
        return UUID.randomUUID().toString()
    }
}