package com.example.bloodbank.Utils

object Constants {
    var VERIFICATION_ID = "verification_id"
    var PHONE_NUMBER = "phone_number"
    var ZONE_LIST = arrayListOf("Bhaktapur","Kathmandu","Lalitpur")

    /**
     * Intent constants
     * */
    var LOCATION:String = "location"
    var BLOOD_GROUP = "blood_group"

    /**
     * Firebase child constants name
     * */
    val REGISTERED_USER = "Registered_User"
    val DONOR_LIST = "DonorList"
    val REQUEST = "Request"

    /**
     * Map
     * */
    const val MAP_API_KEY = "AIzaSyD8afgj4wdtPnNdHeb8Z68EcJ_GOw2B3Hw"
}