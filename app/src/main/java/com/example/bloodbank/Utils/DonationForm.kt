package com.example.bloodbank.Utils

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.example.bloodbank.R

object DonationForm {

    fun showDonationForm(context: Activity, preferenceManager: PreferenceManager) {
        var donationFormDialog: Dialog = Dialog(context)
        donationFormDialog.setContentView(R.layout.dialog_donation_form)

        var rg_age = donationFormDialog.findViewById<RadioGroup>(R.id.rg_age)
        var rg_weight = donationFormDialog.findViewById<RadioGroup>(R.id.rg_weight)
        var rg_disease = donationFormDialog.findViewById<RadioGroup>(R.id.rg_disease)
        var rg_drugs_medicine = donationFormDialog.findViewById<RadioGroup>(R.id.rg_drugs_medicine)
        var rg_medical_surgeries =
            donationFormDialog.findViewById<RadioGroup>(R.id.rg_medical_surguries)
        var tv_error_message = donationFormDialog.findViewById<TextView>(R.id.tv_error_message)
        var btn_apply = donationFormDialog.findViewById<Button>(R.id.btn_apply)
        var btn_cancel = donationFormDialog.findViewById<Button>(R.id.btn_cancel)

        btn_apply.setOnClickListener {
            var checkedButtonIdForAge = rg_age.checkedRadioButtonId
            var checkedButtonIdForWeight = rg_weight.checkedRadioButtonId
            var checkedButtonIdForDisease = rg_disease.checkedRadioButtonId
            var checkedButtonIdForDrugsAndMedicine = rg_drugs_medicine.checkedRadioButtonId
            var checkedButtonIdForMedicalSurgeries = rg_medical_surgeries.checkedRadioButtonId

            if (checkedButtonIdForAge == -1 || checkedButtonIdForWeight == -1 ||
                checkedButtonIdForDrugsAndMedicine == -1 || checkedButtonIdForDisease == -1
                ||checkedButtonIdForMedicalSurgeries==-1){
                tv_error_message.visibility = View.VISIBLE
            }else{
                var ageGroup = rg_age.findViewById<RadioButton>(checkedButtonIdForAge).text.toString()
                var weight = rg_weight.findViewById<RadioButton>(checkedButtonIdForWeight).text.toString()
                var disease = rg_disease.findViewById<RadioButton>(checkedButtonIdForDisease).text.toString()
                var medicalSurgeris = rg_medical_surgeries.findViewById<RadioButton>(checkedButtonIdForMedicalSurgeries).text.toString()
                var drugsAndMedicine = rg_drugs_medicine.findViewById<RadioButton>(checkedButtonIdForDrugsAndMedicine).text.toString()
                val donorFormDao = DonorFormDAO(age = ageGroup,weight = weight,diseases = disease,surgeries = medicalSurgeris,medicine = drugsAndMedicine)
                preferenceManager.saveDonorFormDetails(donorFormDao)
                preferenceManager.saveLastDonationFormFilledDate(DateUtils.getCurrentDate())
                preferenceManager.saveDonationFormIsFilled()
                donationFormDialog.dismiss()
            }

        }
        btn_cancel.setOnClickListener {
            donationFormDialog.dismiss()
        }

        val dialogWindow = donationFormDialog.window
        val m: WindowManager = context.windowManager
        val d = m.defaultDisplay
        assert(dialogWindow != null)
        dialogWindow!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = dialogWindow!!.attributes
        lp.width = (d.width * 0.9).toInt()
        donationFormDialog.setCancelable(false)
        donationFormDialog.show()
    }
}