package com.example.bloodbank.Utils

data class DonorFormDAO(
    val age:String = "",
    val weight:String = "",
    val diseases : String = "false",
    val medicine: String = "false",
    val surgeries:String = "false"
)
