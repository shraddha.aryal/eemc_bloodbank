package com.example.bloodbank.Utils

import java.lang.Math.abs
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun getCurrentDate():String{
        val currentDateTime = Calendar.getInstance().time
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        return sdf.format(currentDateTime)
    }

    fun getDaysDifference(lastDonatedDate: String?):String{
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val date = sdf.parse(lastDonatedDate)
        var currentDate = sdf.parse(getCurrentDate())
        val difference: Long = kotlin.math.abs(currentDate.time - date.time)
        val differenceDates = difference / (24 * 60 * 60 * 1000)
        return  differenceDates.toString()

    }
}