package com.example.bloodbank.Utils

import android.app.Activity
import android.graphics.Typeface
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar


object SnackBar {
    //custom snack bar to show the information
    fun showSnackBar(message: String, layout: CoordinatorLayout) {
        var snack = Snackbar.make(layout, message, Snackbar.LENGTH_LONG)
        snack.show()
    }

    fun showSnackBar(message: String, layout: CoordinatorLayout,color:Int,textColor:Int) {
        var snack = Snackbar.make(layout, message, Snackbar.LENGTH_LONG)
        var view = snack.view
        view.setBackgroundColor(color)
        var textView= snack.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(textColor)
        textView.setTypeface(textView.typeface, Typeface.BOLD)
        snack.show()
    }
}



