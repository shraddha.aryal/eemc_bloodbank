package com.example.bloodbank.Utils

object InputValidation {
    /**
     * This function check if the input field in login and registration is empty or not
     * return false if not empty
     * return true if empty
     * */

    fun checkIsInputEmpty(input: String): Boolean {
        return input.isEmpty()
    }


    fun checkIsEmpty(userId: String, password: String): Boolean =
        (userId.isEmpty() || password.isEmpty())


    fun checkIfPasswordAndConfirmPasswordMatches(
        password: String,
        confirmPassword: String
    ): Boolean {
        return password == confirmPassword
    }

    fun checkUserIdType(userId: String): String {

        if (android.util.Patterns.PHONE.matcher(userId).matches()) {
            return "number"
        }
        return if(userId.contains("@")){
            "email"
        }else "number"
        return "User ID not valid!!"
    }

    fun checkIfEmailIsValid(userId: String):Boolean{
        return android.util.Patterns.EMAIL_ADDRESS.matcher(userId).matches()
    }
    fun checkIfNumberIsValid(userId: String):Boolean{
        return android.util.Patterns.PHONE.matcher(userId).matches()
    }

    fun twoDigitDecimalRoundOff(value:Double):Double{
        return String.format("%.2f", value).toDouble()
    }



}






