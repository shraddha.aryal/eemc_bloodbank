package com.example.bloodbank.Utils

import android.content.Context
import android.content.SharedPreferences
import com.example.bloodbank.donerList.data.User

class PreferenceManager(context: Context) {
    private val preferences: SharedPreferences =
        context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE)
    private val editor: SharedPreferences.Editor = preferences.edit()

    fun saveUserUniqueId(uniqueId: String) {
        editor.putString(USER_UNIQUE_ID, uniqueId).commit()
    }

    fun retrieveUniqueId(): String? {
        return preferences.getString(USER_UNIQUE_ID, "")
    }

    fun saveDonationFormIsFilled() {
        editor.putBoolean(IS_DONATION_FORM_FILLED, true).commit()
    }

    fun retrieveDonationFormFilledStatus(): Boolean {
        return preferences.getBoolean(IS_DONATION_FORM_FILLED, false)
    }

    fun saveLastDonationFormFilledDate(date: String) {
        editor.putString(LAST_DONATION_FORM_FILLED_DATE, date).commit()
    }

    fun retrieveLastDonationFormFilledDate(): String? {
        return preferences.getString(LAST_DONATION_FORM_FILLED_DATE,"")
    }

    fun saveUserDetails(user: User) {
        editor.putString(BLOOD_GROUP, user.bloodGroup).commit()
        editor.putString(CONTACT_NUMBER, user.contactNumber).commit()
        editor.putBoolean(IS_DONOR, user.donor).commit()
        editor.putString(LOCATION, user.location).commit()
        editor.putString(USER_ID, user.userId).commit()
        editor.putString(USER_UNIQUE_ID, user.userUniqueId).commit()
        editor.putString(USERNAME, user.username).commit()
        editor.putString(LAST_DONATED_DATE, user.lastDonated).commit()
        editor.putInt(DONATION_COUNT, user.donationCount).commit()
    }

    fun retrieveUserDetails(): User {
        return User(
            username = preferences.getString(USERNAME, "")!!,
            contactNumber = preferences.getString(CONTACT_NUMBER, "")!!,
            userId = preferences.getString(USER_ID, "")!!,
            bloodGroup = preferences.getString(BLOOD_GROUP, "")!!,
            location = preferences.getString(LOCATION, "")!!,
            userUniqueId = preferences.getString(USER_UNIQUE_ID, "")!!,
            donor = preferences.getBoolean(IS_DONOR, false),
            lastDonated = preferences.getString(LAST_DONATED_DATE, "N/A"),
            donationCount = preferences.getInt(DONATION_COUNT, 0)
        )
    }

    fun updateLastDonatedDate(currentDate: String) {
        editor.putString(LAST_DONATED_DATE, currentDate).commit()
    }

    fun retrieveDonorFormDetails():DonorFormDAO{
        return DonorFormDAO(
            age = preferences.getString(FORM_AGE,"")!!,
            diseases = preferences.getString(FORM_DISEASES,"")!!,
            medicine = preferences.getString(FORM_MEDICINES,"")!!,
            surgeries = preferences.getString(FORM_SURGERIES,"")!!,
            weight = preferences.getString(FORM_WEIGHT,"")!!
        )
    }
    fun saveDonorFormDetails(donorFormDao: DonorFormDAO) {
        editor.putString(FORM_AGE,donorFormDao.age).commit()
        editor.putString(FORM_DISEASES,donorFormDao.diseases).commit()
        editor.putString(FORM_MEDICINES,donorFormDao.medicine).commit()
        editor.putString(FORM_SURGERIES,donorFormDao.surgeries).commit()
        editor.putString(FORM_WEIGHT,donorFormDao.weight).commit()
    }

    companion object {
        private const val PREFERENCE_NAME = "blood_bank"
        private const val PRIVATE_MODE = 0
        private const val IS_DONATION_FORM_FILLED = "is_donation_form_filled"
        private const val LAST_DONATION_FORM_FILLED_DATE = "last_donation_form_filled_date"

        /**
         * User Details
         * */
        private const val BLOOD_GROUP = "blood_group"
        private const val CONTACT_NUMBER = "contactNumber"
        private const val IS_DONOR = "is_donor"
        private const val LOCATION = "location"
        private const val USER_ID = "user_id"
        private const val USER_UNIQUE_ID = "user_unique_id"
        private const val USERNAME = "username"
        private const val LAST_DONATED_DATE = "last_donated_date"
        private const val DONATION_COUNT = "donation_count"

        /**
         * Donation Form
         * */
        private const val FORM_AGE ="form_age"
        private const val FORM_WEIGHT ="form_weight"
        private const val FORM_DISEASES ="form_diseases"
        private const val FORM_MEDICINES = "form_medicines"
        private const val FORM_SURGERIES = "form_surgeries"

    }
}