package com.example.bloodbank.Utils

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import com.example.bloodbank.R
import com.example.bloodbank.donerList.data.User
import com.example.bloodbank.location.LocationUtils
import com.example.bloodbank.maps.view.MapActivity
import com.example.bloodbank.maps.enum.MapEnum

object NotificationUtils {
    const val REQUEST_NOTIFICATION_CHANNEL = "request_channel_notification"
    const val REQUEST_NOTIFICATION_DESCRIPTION =
        "This channel will deliver notification of blood request to donor."
    const val REQUEST_CHANNEL_ID = "Blood_request_channel"
    const val REQUEST_NOTIFICATION_ID = 0

    fun showRequestNotification(
        context: Context,
        donor: User

    ): Notification {
        createNotificationChannel(context)
      return setupBuilder(context,donor)
    }

    private fun createNotificationChannel(context: Context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = REQUEST_NOTIFICATION_CHANNEL
            val descriptionText = REQUEST_NOTIFICATION_DESCRIPTION
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(REQUEST_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun setupBuilder(
        context: Context,
        user: User
    ): Notification {
        var mapIntent = Intent(context, MapActivity::class.java)
        var bundle = Bundle()
        bundle.putSerializable(MapEnum.User.value, user)
        mapIntent.putExtra("bundle",bundle)
        val message ="${user!!.username} has requested for ${user!!.bloodGroup} blood from ${user.location}"
        val title = "${user!!.bloodGroup} Blood Request"
        val mapPendingIntent: PendingIntent? =  PendingIntent.getActivity(context, 0, mapIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        return NotificationCompat.Builder(context, REQUEST_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_blood_drop_)
            .setContentTitle(title)
            .setContentIntent(mapPendingIntent)
            .setContentText(message)
            .setColor(Color.RED)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
    }

    fun showCancelNotification(mContext: Context, donorName: String?): Notification? {
        return setupRequestCancelNotificationBuilder(mContext, donorName!!)
    }

    private fun setupRequestCancelNotificationBuilder(context: Context, donorName:String): Notification? {
        val message ="$donorName has cancelled your blood request"
        val title = "Blood Request Cancelled"
        return NotificationCompat.Builder(context, REQUEST_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_blood_drop_)
            .setContentTitle(title)
            .setContentText(message)
            .setColor(Color.RED)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
    }

    fun showAcceptedNotification(mContext: Context,user:User): Notification? {
        return setupAccetedRequestNotificationBuilder(mContext, user)
    }

    private fun setupAccetedRequestNotificationBuilder(mContext: Context, user: User): Notification? {
        val message ="Donor current location is ${LocationUtils.getCurretntLocation(mContext,user.latitude,user.longitude)}.\nTap to check the donor location in realtime."
        val title = "${user.donorName} has accepted your blood request."
        var mapIntent = Intent(mContext, MapActivity::class.java)
        var bundle = Bundle()
        bundle.putSerializable(MapEnum.User.value, user)
        mapIntent.putExtra("bundle",bundle)
        val mapPendingIntent: PendingIntent? =  PendingIntent.getActivity(mContext, 0, mapIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        return NotificationCompat.Builder(mContext, REQUEST_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_blood_drop_)
            .setContentTitle(title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(message))
            .setColor(Color.RED)
            .setContentIntent(mapPendingIntent)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
    }
}