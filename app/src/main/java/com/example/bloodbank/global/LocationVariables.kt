package com.example.bloodbank.global

import androidx.lifecycle.MutableLiveData

object LocationVariables {
     var latitude : Double = 27.6911981025713
     var longitude : Double = 85.36869520490207
     var searchRadius:Double= 1200.00
     var searchRadiusMaxValue = 1200.00

     var mutableLatitude:MutableLiveData<Double> =  MutableLiveData(27.6911981025713)
     var mutableLongitude:MutableLiveData<Double> =   MutableLiveData(85.36869520490207)
}